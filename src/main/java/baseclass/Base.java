package baseclass;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;

import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Selenide.close;

public class Base {

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(ITestContext context) {

        for (ITestNGMethod method : context.getAllTestMethods()) {
            method.setRetryAnalyzer(new RetryAnalyzer());
        }

        WebDriverRunner.isChrome();
        Configuration.browser = "CHROME";
        Configuration.reopenBrowserOnFail = true;

        ChromeDriverManager.getInstance().setup();

//        EdgeDriverManager.getInstance().setup();
//        FirefoxDriverManager.getInstance().setup();

    }

    @AfterMethod (alwaysRun = true)
    public void AfterMethod(){
        close();
    }

    public static void setElementText(SelenideElement element, String text){
        element.isDisplayed();
        element.clear();
        element.setValue(text);
        element.shouldHave(value(text));
    }

    public static void pressing(SelenideElement element){
        element.isDisplayed();
        element.click();
    }

    public static void clickElement(SelenideElement element){
        element.click();
    }

    public static void setDropdown(SelenideElement element, String text) {
        new Select(element).selectByVisibleText(text);
    }

}