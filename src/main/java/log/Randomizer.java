package log;

import java.util.Random;

class Randomizer {

    static int randomNumber() {
        Random rand = new Random();
        return rand.nextInt((1000000 - 1) + 1) + 1;
    }
}