package log;

import com.codeborne.selenide.SelenideElement;

import static baseclass.Base.setElementText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static log.Values.validLogin;
import static log.Values.validPassword;
import static org.openqa.selenium.By.partialLinkText;
import static org.openqa.selenium.By.xpath;
import static pageObject.BaseObjects.homePageDev;

public class shortCuts {

    private static final SelenideElement usernameField = $("#id_username");
    private static final SelenideElement passwordField = $("#id_password");
    private static final SelenideElement submitButton  = $(xpath(".//*[@id='login_form']/button"));

    public static void logging() {
        open(homePageDev + "/auth/login/");
        setUsernameField(validLogin);
        setPasswordField(validPassword);
        submitButton.click();
        sleep(2000);
    }

    private static void setUsernameField(String name){
        setElementText(usernameField, name);
    }

    private static void setPasswordField(String name){
        passwordField.isDisplayed();
        passwordField.clear();
        passwordField.setValue(name);
    }

    public static void openByLinkText(String selector) {
        $(partialLinkText(selector)).click();
        sleep(1000);
    }

    public static void assertURL(String url) {
        url().equalsIgnoreCase(url);
    }

    public static void assertTitle(String title) {
        title().equalsIgnoreCase(title);
    }

}
