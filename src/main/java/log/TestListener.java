package log;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    static MyTestResult result = new MyTestResult();

    @Override
    public void onTestSuccess(ITestResult var1){
        System.out.println(var1.getName() + " was successful."+"\n");
        result.incrementPassed();
        System.out.println("PASSED TESTS: " + result.getPassed()+"\n");
        System.out.println("TOTAL TESTS: " + result.getTotalTests()+"\n");
    }

    @Override
    public void onTestFailure(ITestResult var1){
        System.out.println(var1.getName() + " was a failure. \n Throwable: " + var1.getThrowable().getMessage() + "\n");
        result.incrementFailed();
        System.out.println("FAILED TESTS: " + result.getFailed() +"\n");
        System.out.println("TOTAL TESTS: " + result.getTotalTests()+"\n");

    }

    @Override
    public void onTestSkipped(ITestResult var1){
        System.out.println(var1.getName() + " was skipped."+"\n");
        result.incrementSkipped();
        System.out.println("SKIPPED TESTS: " + result.getSkipped()+"\n");
        System.out.println("TOTAL TESTS: " + result.getTotalTests()+"\n");
    }

    @Override
    public void onFinish(ITestContext var1){
        try {
            result.writeTXT();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
