package log;

import static log.Randomizer.randomNumber;

public class Values {

    public static String validLogin    = "test.user@divio.ch";
    public static String validPassword = "rPm-xms-b44-oHX";

    public static String newUserLogin  = "test_user" + randomNumber() + "@mailforspam.com";

    public static String name          = "name" + randomNumber();
    public static String packageName   = "package" + randomNumber();
    public static String boilerplateID = "boilerpalterID" + randomNumber();
    public static String portfolioName = "portfolioName" + randomNumber();
    public static String projectName   = "projectName" + randomNumber();
    public static String description   = "Cras mattis consectetur purus sit amet fermentum.";
}
