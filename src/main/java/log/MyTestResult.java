package log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class MyTestResult {

    int passed;
    int failed;
    int skipped;

    public int getPassed(){
        return this.passed;
    }

    public int getFailed(){
        return this.failed;
    }

    public int getSkipped(){
        return this.skipped;
    }

    public int getTotalTests(){
        return (this.passed + this.failed + this.skipped);
    }

    public void incrementPassed(){
        this.passed++;
    }

    public void incrementFailed(){
        this.failed++;
    }

    public void incrementSkipped(){
        this.skipped++;
    }

    public void writeTXT() throws Exception{
        File file = new File("testResults.txt");

        try (FileWriter fw = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fw)){
                bufferedWriter.write("PASSED="+this.passed+"\n");
                bufferedWriter.write("FAILED="+this.failed+"\n");
                bufferedWriter.write("SKIPPED="+this.skipped+"\n");
                bufferedWriter.write("TOTAL="+this.getTotalTests()+"\n");
            }
        }

    public void writeCSV() throws Exception{
        File file = new File("testResults.csv");

        try (FileWriter fw = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fw)){
            bufferedWriter.write("PASSED="+this.passed+"\n");
            bufferedWriter.write("FAILED="+this.failed+"\n");
            bufferedWriter.write("SKIPPED="+this.skipped+"\n");
            bufferedWriter.write("TOTAL="+this.getTotalTests()+"\n");
        }
    }
}