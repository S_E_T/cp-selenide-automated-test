package pageObject.Headers;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.xpath;

public interface Billing extends BaseObjects {

    SelenideElement addCard     = $(".form-control-static.js-payment-button");
    SelenideElement textInModal = $(xpath("html/body/div[2]/section/span[2]/div/div/main/div/header/h1"));

    static void pressAddcard(){
        pressing(addCard);
    }
}
