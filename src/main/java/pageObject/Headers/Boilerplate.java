package pageObject.Headers;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import java.util.ArrayList;
import java.util.List;

import static baseclass.Base.setElementText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static log.shortCuts.logging;
import static log.shortCuts.openByLinkText;
import static org.openqa.selenium.By.xpath;

public interface Boilerplate extends BaseObjects {

    SelenideElement identifier                = $("#id_identifier");
    SelenideElement descriptionField          = $("#id_description");
    SelenideElement titleName                 = $(".h2.control-title.control-title-settings");
    SelenideElement nameAlreadyUsed           = $("div.form-group.has-error > span.help-block > div.help-block");
    SelenideElement infoTooltip               = $(".ui-widget.ui-widget-content.ui-corner-all");
    SelenideElement tabTitle                  = $(".panel-title.h2");
    SelenideElement collaborabotsInput        = $("ul.select2-selection__rendered");
    SelenideElement collaborabotsInputMessage = $("li.select2-results__option.select2-results__message");
    SelenideElement dropdownItem              = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div/form/div[1]/div[6]/select/option[3]"));

    SelenideElement createButton = $(".btn.btn-primary.pull-right");

    List<String> usedItems = new ArrayList<>();

    static void logAndFindBoilerplateTab() throws Exception {
        logging();
        openByLinkText("Boilerplate");
        sleep(2000);
    }

    static void findFisrtBoilerplate() throws Exception {
        openByLinkText("Boilerplate");
        sleep(2000);
        $(xpath(".//*[@id='page-top']/div[2]/div[2]/div/ul/li[1]/div/div[5]/a")).click();
    }

    static void setIdentifier(String name) {
      setElementText(identifier, name);
    }

    static void setDescriptionField(String name) {
       setElementText(descriptionField, name);
    }

    static void getUsedItems() {
        String addonName = nameField.getValue();
        String packageName = packageNameField.getValue();
        usedItems.add(addonName);
        usedItems.add(packageName);
    }

}
