package pageObject.Headers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.*;
import static com.codeborne.selenide.Selenide.*;
import static log.shortCuts.logging;
import static log.shortCuts.openByLinkText;
import static org.openqa.selenium.By.xpath;
import static pageObject.Headers.Profile.selectExpertise;

public interface Profile extends BaseObjects {

    SelenideElement urlField         = $("#id_url");
    SelenideElement emailField       = $("#id_contact_email");
    SelenideElement descriptionField = $(".cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders>p");
    SelenideElement twitterField     = $("#id_twitter_handle");
    SelenideElement titleField       = $("#id_title");
    SelenideElement activeSinse       = $("#id_active_since");
    SelenideElement secondOption      = $("#id_active_since>option:nth-child(2)");
    SelenideElement firstOption       = $("#id_active_since>option:nth-child(1)");
    SelenideElement sixteenthOption   = $("#id_active_since>option:nth-child(16)");
    SelenideElement builtWithDjango   = $("#id_built_with_django_cms");
    SelenideElement screenshot        = $("#id_screenshot");
    SelenideElement refreshScreenshot = $("#id_refresh_screenshot");

    SelenideElement locationField     = $(".form-control.js-geocomplete");
    SelenideElement cancelButton      = $(".btn.btn-default.js-modal-close");
    SelenideElement openChangeMenu    = $(".btn-group.pull-right.location-controls");
    SelenideElement expertise         = $(".checkbox > label");
    SelenideElement canceling         = $(".btn.btn-default.js-history-back");
    SelenideElement itemsName         = $("h3.reference-item-title");
    SelenideElement itemsImage        = $(".reference-item-image.img-responsive");
    SelenideElement trashIcon         = $("i.icon.icon-trash");
    SelenideElement change            = $(".btn-group.pull-right.location-controls > .dropdown-toggle");
    SelenideElement edit              = $(".js-content-to-modal > .icon.icon-pencil");
    SelenideElement deleteButton      = $("#remove-reference-form > div.panel-footer.clearfix > div.pull-right > button.btn.btn-primary");
    SelenideElement addButton         = $(".pull-right > .btn.btn-primary.js-processing-btn");
    SelenideElement suggestion        = $(".pac-item");
    SelenideElement experty           = $(".checkbox>label");

//    TODO check it
    SelenideElement deleteAvatarPic      = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div/div/a/i"));
    SelenideElement pictureAvatarUpload  = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div/div/div/div/div/input[2]"));
    SelenideElement deleteCoverPicture   = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div/div/a/i"));
    SelenideElement pictureCoverUpload   = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div/div/div/div/div/input[2]"));

//    TODO check it
    SelenideElement goToLocations        = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[2]/div/div/div[2]/a"));
    SelenideElement AdditionalInfo       = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[3]/div/div/div[2]/a"));
    SelenideElement selectRemove         = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[2]/div[2]/div[1]/div[1]/div[1]/ul/li[3]/a"));
    SelenideElement editIcon             = $(xpath("//body[@id='page-top']/div[2]/div[2]/div/div[2]/div/div[4]/div[2]/div/div/div/div[3]/a/i"));

    SelenideElement Portfolio            = $(xpath("//body[@id='page-top']/div[2]/div[2]/div/div[2]/div/div[4]/div/div/div[2]/a"));
    SelenideElement CancelButtonInModal  = $(xpath("html/body/div[5]/div/div/div/div/form/div[2]/div/a"));
    SelenideElement deleteLocationButton = $(xpath("html/body/div[5]/div/div/div/div/form/div[2]/div/button"));
    SelenideElement tagsField            = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[3]/form/div[1]/div[2]/div/div/div/input"));

    ElementsCollection locationsArray = $$(".location-address");
    ElementsCollection portfolioItems = $$(".reference-item-title");


    String file = "/Users/serhiisydorenko/Desktop/Divio/SelenideV3divio/src/main/resources/gif45.gif";
//    int last;

    static void logAndFindProfileTab() {
        logging();
        sleep(1500);
        openByLinkText("Profile");
        sleep(500);
    }

    static void goToAddInfo(){
        pressing(AdditionalInfo);
   }

    static void goToPorfolio(){
        pressing(Portfolio);
    }

    static void setUrlField(String name) {
       setElementText(urlField, name);
    }

    static void setEmailField(String name) {
       setElementText(emailField, name);
    }

    static void setDescriptionField(String name) {
        setElementText(descriptionField, name);
    }

    static void setTwitterField(String name) {
      setElementText(twitterField, name);
    }

    static void setTitleField(String name) {
        setElementText(titleField, name);
    }

    static void setNewLocation(String name) {
        setElementText(locationField,name);
    }

    static void deleteAvatarPicture(){
        pressing(deleteAvatarPic);
        sleep(1000);
    }

    static void uploadAvatarPiture(){
        pictureAvatarUpload.sendKeys(file);
        sleep(2000);
    }

    static void deleteCoverPicture(){
        pressing(deleteCoverPicture);
        sleep(1000);
    }

    static void uploadCoverPicture(){
        pictureCoverUpload.sendKeys(file);
        sleep(2000);
    }

    static void GoToLocations(){
        pressing(goToLocations);
    }

    static void pressCancelButton(){
        pressing(cancelButton);
    }

    static void DeleteLocation() {
        clickElement(openChangeMenu);
        clickElement(selectRemove);
        clickElement(deleteLocationButton);
    }

    static void selectExpertise(){
        pressing(expertise);
        sleep(1000);
    }

    static void uploadScreenshot(String file) {
        screenshot.clear();
        screenshot.sendKeys(file);
    }

    static void pressEditIcon() {
        clickElement(editIcon);
    }

    static void refreshScreenshot() {
        clickElement(refreshScreenshot);
    }

    static void pressTrashIcon() {
        clickElement(trashIcon);
        sleep(1000);
    }

    static void pressCancelInModal() {
        pressing(CancelButtonInModal);
    }

    static void pressDeleteButton() {
        pressing(deleteButton);
    }

    static void pressAddButton() {
        pressing(addButton);
        sleep(2000);
    }

    static void selectSuggestion() {
        pressing(suggestion);
        sleep(2000);
    }

    static void pressChange() {
        pressing(openChangeMenu);
    }

    static void pressEdit() {
        pressing(edit);
    }

    static void makeSureThatNotSelected() {

        if ( experty.has(Condition.cssClass("checked"))) {
            selectExpertise();
            BaseObjects.pressSaveButton();
        } else {}

    }
}