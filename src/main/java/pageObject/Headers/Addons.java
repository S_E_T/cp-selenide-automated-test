package pageObject.Headers;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import java.util.ArrayList;
import java.util.List;

import static baseclass.Base.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static log.shortCuts.openByLinkText;
import static org.openqa.selenium.By.xpath;

public interface Addons extends BaseObjects {

    SelenideElement projectURL                = $("#id_project_url");
    SelenideElement pypiURL                   = $("#id_pypi_url");
    SelenideElement docsURL                   = $("#id_docs_url");
    SelenideElement makeAddonPublic           = $("#id_make_public");
    SelenideElement authorNameField           = $("#id_owner_display_name");
    SelenideElement versionOfPackage          = $("#id_default_release_channel");
    SelenideElement repositoryURL             = $("#id_repository_url");
    SelenideElement publishedText             = $(".alert.alert-success");
    SelenideElement titleName                 = $(".h2.control-title.control-title-settings");
    SelenideElement addonsTabName             = $("div.panel-main-title.pull-left");
    SelenideElement successMessage            = $("ui-pnotify-text");
    SelenideElement sujectedCompatables       = $("div.tt-suggestion.tt-selectable");
    SelenideElement selectedCompatable        = $(".tag.label.label-info");
    SelenideElement collaboratorsResult       = $("li.select2-results__option.select2-results__message");
    SelenideElement inputForContatableProject = $("input.tt-input");
    SelenideElement firstAddon                = $("li.list-group-item:nth-child(1) > div:nth-child(1) > div:nth-child(5) > a:nth-child(1)");
    SelenideElement youTubeField              = $("#id_videos-0-video");
    SelenideElement compatableProject         = $("div.bootstrap-tagsinput");
    SelenideElement collaboratorsDropdown     = $("ul.select2-selection__rendered");
    SelenideElement compatableProjectsItems   = $(xpath("//div[@id='servers']/div[2]/form/div/div[3]/div/span/span"));
    SelenideElement nameAlreadyUsed           = $(xpath("html/body/div[2]/div[2]/div/div/form/div/div[1]/div[2]/span/div[1]"));
    SelenideElement selectedDropdownItem      = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[2]/form/div[1]/div[7]/select/option[3]"));
    SelenideElement selectedVersionOfPackage  = $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div/div[2]/form/div[1]/div[2]/select/option[1]"));

    List<String> usedItems = new ArrayList<>();

    static void setAuthorName(String name) {
        setElementText(authorNameField, name);
    }

    static void setYouTubeField(String name) {
        setElementText(youTubeField, name);
    }

    static void setRepositoryURL(String name) {
        setElementText(repositoryURL, name);
    }

    static void setProjectURL(String name) {
       setElementText(projectURL, name);
    }

    static void setPypiURL(String name) {
       setElementText(pypiURL, name);
    }

    static void setDocsURL(String name) {
        setElementText(docsURL, name);
    }

    static void goToPackageInformation() {
        openByLinkText("Package Information");
        addonsTabName.should(text("Package Information"));
    }

    default void getFirstAddon() {
        openByLinkText("Addons");
        sleep(1500);
        pressing(firstAddon);
        sleep(1500);
    }

    static void getUsedItems() {
        String addonName = nameField.getValue();
        String packageName = packageNameField.getValue();
        usedItems.add(addonName);
        usedItems.add(packageName);
    }

    static void makeAddonPublic() {
        makeAddonPublic.click();
    }

    static void selectCompatableProject() {
        clickElement(compatableProject);
    }

    static void deleteAllCompatibleProjectsTypes() {
        clickElement(compatableProjectsItems);
    }

    static void openCollaboratorsDropdown() {
        clickElement(collaboratorsDropdown);
    }
}
