package pageObject.homePage;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static baseclass.Base.setElementText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public interface registerPageObject extends BaseObjects {

    String RegisterPage = "https://control.divio.com/auth/register/";

    SelenideElement createAccountButton = $(".btn.btn-marketing.btn-block.btn-lg.js-analytics-link");
    SelenideElement message      = $(".panel-body>h2");
    SelenideElement popUp        = $("div.help-block");
    SelenideElement emailField   = $("#id_email");

    static void setEmailField(String name) {
        setElementText(emailField, name);
    }

    static void messageIsPreset(String text) {
        message.shouldHave(text(text));
    }

    static void popUpIsPreset(String text) {
        popUp.shouldHave(text(text));
    }

    static void pressCreateButton(){
        pressing(createAccountButton);
        sleep(2000);

    }

}
