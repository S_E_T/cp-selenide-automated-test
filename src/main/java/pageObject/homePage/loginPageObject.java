package pageObject.homePage;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.DataProvider;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static baseclass.Base.setElementText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.xpath;
import static pageObject.homePage.homepageObject.openAccountDropdown;

public interface loginPageObject extends BaseObjects {

    SelenideElement usernameField = $("#id_username");
    SelenideElement passwordField = $("#id_password");
    SelenideElement submitButton  = $(xpath(".//*[@id='login_form']/button"));
    SelenideElement CenterText    = $("h1.text-center");
    SelenideElement GoogleLink    = $(".btn.btn-default.js-analytics-link");
    SelenideElement GitHubLink    = $(".row .col-md-12:last-child .btn.btn-default.js-analytics-link");
    SelenideElement logo          = $(".navbar-brand>a");
    SelenideElement googleLogo    = $(".logo.logo-w");
    SelenideElement validationErrorMessage = $(".alert.alert-danger.alert-dismissable.alert-link");

    @DataProvider(name = "links")
    static Object[][] links() {

        return new Object[][]{
                {"Privacy", "OUR PRIVACY POLICY"},
                {"Security", "SECURITY"},
                {"Site Notice", "SITE NOTICE"},
                {"Terms of Service", "TERMS OF SERVICE"}
        };
    }

    static void CenterTextIsValid() {
        CenterText.shouldHave(text("Sign in to Divio Control Panel"));
    }

    static void setUserName(String name) {
       setElementText(usernameField, name);
    }

    static void setPassword(String name) {
        setElementText( passwordField, name);
    }

    static void pressSubmitButton() {
        pressing(submitButton);
    }

    static void validationErrorMessageIsPresent() {
        validationErrorMessage.shouldHave(text("Please enter a correct email and password. Note that both fields may be case-sensitive."));
    }

    static void logout() {
        openAccountDropdown();
        sleep(3000);
        $(linkText("Logout")).click();
    }

    static void signInWithGoogle() {
        pressing(GoogleLink);
    }

    static void signInWithGitHib() {
        pressing(GitHubLink);
    }

    static void clickOnLogo() {
        pressing(logo);
    }

}