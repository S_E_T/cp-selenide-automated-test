package pageObject.homePage;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public interface resetPage extends BaseObjects {

    String ResetPasswordPage    = "https://control.divio.com/auth/account/reset-password/";

    SelenideElement resetButton = $("#submit");
    SelenideElement message     = $(".alert.alert-warning");

     static void pressReset() {
        pressing(resetButton);
    }

    static void messageContains(String text) {
        message.shouldHave(text(text));
    }

}
