package pageObject.homePage;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.DataProvider;
import pageObject.BaseObjects;

import static baseclass.Base.clickElement;
import static baseclass.Base.pressing;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public interface homepageObject extends BaseObjects {

    SelenideElement contextDropdown = $(".btn.btn-default.btn-context-toggle.dropdown-toggle");
    SelenideElement helpDropdown    = $(".btn.btn-block.btn-header.dropdown-toggle");
    SelenideElement accountDropdown = $(".btn.btn-header.dropdown-toggle.account-dropdown");
    SelenideElement h1              = $("div.holder.container > h1");
    SelenideElement search          = $(".icon.icon-search.icon-lg");
    SelenideElement tooltip         = $(".icon.icon-lightbulb-o.icon-lg");
    SelenideElement tooltipText     = $(".col-md-15>h3");
    SelenideElement dropdownSign    = $("div.sign-out-meta");

    String help    = "html/body/div[2]/header/div/div/div/div[3]/div/ul/li[2]/div/ul/li[1]/a";
    String support = "html/body/div[2]/header/div/div/div/div[3]/div/ul/li[2]/div/ul/li[3]/a";
    String status  = "html/body/div[2]/header/div/div/div/div[3]/div/ul/li[2]/div/ul/li[5]/a";

    static void openContextDropdown() {
        pressing(contextDropdown);
    }

    static void openHelpDropdown() {
        pressing(helpDropdown);
    }

    static void openAccountDropdown() {
        pressing(accountDropdown);
    }

    static void assertName(String text) {
        h1.shouldHave(text(text));
    }

    static void openSearchDropdown() {
        clickElement(search);
    }

    @DataProvider(name = "helpDropdown")
    static Object[][] helpDropdown() {
        return new Object[][]{
                {support, "http://support.divio.com/"},
                {status, "http://status.divio.com/"},
                {help, "https://www.divio.com/en/solutions/expert-help/"}
        };
    }

    @DataProvider(name = "tabs")
    static Object[][] tabs() {
        return new Object[][]{
                {"Projects", "https://control.divio.com/control/"},
                {"Addons", "https://control.divio.com/account/my-addons/"},
                {"Boilerplates", "https://control.divio.com/account/my-boilerplates/"},
                {"Profile", "https://control.divio.com/account/profile/"},
                {"Billing", "https://control.divio.com/account/billing/"}
        };
    }

    @DataProvider(name = "accountDropdown")
    static Object[][] accountDropdown() {
        return new Object[][]{
                {"Product changes", "https://control.divio.com/account/contact/"},
                {"Account settings", "https://control.divio.com/control/product-changes/"},
        };
    }

    static void openTooltip() {
        clickElement(tooltip);
        clickElement(tooltip);
    }
}