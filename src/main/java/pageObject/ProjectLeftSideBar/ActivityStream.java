package pageObject.ProjectLeftSideBar;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.xpath;
import static pageObject.ProjectLeftSideBar.Addons.UpgradingTheAddon;

public interface ActivityStream extends BaseObjects {

    String project = "https://control.divio.com/control/1073/edit/32155/";

    SelenideElement addons           = $(".js-addons > a");
    SelenideElement checkLog         = $(".col-md-12>small>a");
    SelenideElement showDiff         = $(".col-md-12>small>a:last-child");
    SelenideElement textOfDeploy     = $(".panel-body>pre");
    SelenideElement showGetDifButton = $(".git-commit-item > .pull-right > .btn.btn-default");
    SelenideElement header           = $(".panel-title.h2");
    SelenideElement title            = $(".d2h-file-list-wrapper");
    SelenideElement cautionText      = $(".alert.alert-info.js-show-on-bp-rc-change");

    SelenideElement configure        = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div/ul/form/li[2]/div/div[4]/a"));
    SelenideElement updateAddon      = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div/ul/form/li[2]/div/div[4]/a"));
    SelenideElement changeVersion    = $(xpath("html/body/div[2]/div/div[1]/div[2]/div/div/form/div[2]/a[2]"));
    SelenideElement downgrade        = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div/div[3]/ul/li[2]/div/div[3]/a"));
    SelenideElement updateButton     = $(xpath("html/body/div[2]/div/div[1]/div[2]/div/div/form/div[2]/div/button"));
    SelenideElement yesDowngrade     = $(xpath("html/body/div[2]/div/div[1]/div[2]/div/div/form/div[2]/div/button"));





    static void getProject() {
        open(project);
    }

    static void configureAnAddon() {

        if(configure.isDisplayed()) {
            Addons.DowngradingTheAddon();
        }else {
            UpgradingTheAddon();
        }
    }

    static void goToAddons(){
        pressing(addons);
    }

    static void openCheckLog() {
        pressing(checkLog);
    }

    static void openShowDiff() {
        pressing(checkLog);
    }

    static void windowIsOpenedAndHasAText() {
        textOfDeploy.getText().contains("===== docker build =====");
    }

    static void pressDiffButton() {
        pressing(showGetDifButton);
    }

    static void changeArePresent() {
        title.shouldBe(visible);
    }


}

