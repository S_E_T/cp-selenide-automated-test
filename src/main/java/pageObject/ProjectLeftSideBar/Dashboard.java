package pageObject.ProjectLeftSideBar;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.xpath;

public interface Dashboard extends BaseObjects {

    SelenideElement deployTestServerButton = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div[1]/div/div[1]/div/div/div[3]/div/div/div/div[1]/button"));
//    SelenideElement massegeIsPresent = $(".alert.alert-success");



    static void deployTestServer() {
        pressing(deployTestServerButton);
    }
}
