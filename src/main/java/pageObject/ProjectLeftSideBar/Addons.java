package pageObject.ProjectLeftSideBar;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static baseclass.Base.pressing;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.xpath;
import static pageObject.ProjectLeftSideBar.Dashboard.deployTestServerButton;

public interface Addons extends BaseObjects {

    SelenideElement massegeIsPresent = $(".alert.alert-success");

    SelenideElement configure        = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div/ul/form/li[2]/div/div[4]/a"));
    SelenideElement updateAddon      = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div/ul/form/li[2]/div/div[4]/div/a"));
    SelenideElement changeVersion    = $(xpath("html/body/div[2]/div/div[1]/div[2]/div/div/form/div[2]/a[2]"));
    SelenideElement downgrade        = $(xpath("html/body/div[2]/div[2]/div[1]/div[2]/div/div/div[3]/ul/li[2]/div/div[3]/a"));
    SelenideElement updateButton     = $(xpath("html/body/div[2]/div/div[1]/div[2]/div/div/form/div[2]/div/button"));

    SelenideElement succesfulMessage = $(".ui-widget.ui-widget-content.ui-corner-all.ui-pnotify-container.ui-state-default.ui-pnotify-shadow");
    SelenideElement status           = $(".project-deploy-status");

    static void DowngradingTheAddon(){
        pressing(configure);
        pressing(changeVersion);
        pressing(downgrade);
        pressing(updateButton);
        succesfulMessage.shouldBe(visible);
    }

    static void UpgradingTheAddon(){
        pressing(updateAddon);
        pressing(updateButton);
        succesfulMessage.shouldBe(visible);
    }

    static void  getStatus() {
        status.getText();
    }

    static void DeployTestServer() {

        if (deployTestServerButton.isDisplayed()) {
            pressing(deployTestServerButton);
            massegeIsPresent.waitUntil(visible, 180000, 10000);
            massegeIsPresent.shouldBe(visible);
        }
        else {
            deployTestServerButton.waitUntil(visible, 180000, 10000);
            pressing(deployTestServerButton);
            massegeIsPresent.waitUntil(visible, 180000, 10000);
            massegeIsPresent.shouldBe(visible);
        }
    }

}
