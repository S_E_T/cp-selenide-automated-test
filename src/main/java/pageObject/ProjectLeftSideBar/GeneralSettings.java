package pageObject.ProjectLeftSideBar;

import com.codeborne.selenide.SelenideElement;
import pageObject.BaseObjects;

import static com.codeborne.selenide.Selenide.$;

public interface GeneralSettings extends BaseObjects {

    SelenideElement releaseChannen = $("#id_base_project_release_channel");

}
