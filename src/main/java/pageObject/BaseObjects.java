package pageObject;

import com.codeborne.selenide.SelenideElement;

import static baseclass.Base.pressing;
import static baseclass.Base.setDropdown;
import static baseclass.Base.setElementText;
import static com.codeborne.selenide.Selenide.$;

public interface BaseObjects {

    String homePageDev = "https://control.dev.aldryn.net";
    String homePageLive = "http://control.divio.com";

    SelenideElement nameField        = $("#id_name");
    SelenideElement packageNameField = $("#id_package_name");
    SelenideElement licenseDropdown  = $("#id_license");

    SelenideElement createButton     = $(".btn.btn-primary.pull-right");
    SelenideElement saveButton       = $(".btn.btn-primary.js-processing-btn");
    SelenideElement succesfulMessage = $(".ui-widget.ui-widget-content.ui-corner-all.ui-pnotify-container.ui-state-default.ui-pnotify-shadow");

    static void pressSaveButton() {
        pressing(saveButton);
    }

    static void pressCreateButton() {
        pressing(createButton);
    }

    static void setNameField(String name) {
        setElementText(nameField, name);
    }

    static void setPackageName(String name) {
        setElementText(packageNameField, name);
    }

    static void setLicenseDropdown(String optionName) {
        setDropdown(licenseDropdown, optionName);
    }

}