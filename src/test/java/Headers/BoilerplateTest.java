package Headers;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.*;
import pageObject.BaseObjects;
import pageObject.Headers.Boilerplate;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.sleep;
import static log.Values.*;
import static log.Values.name;
import static log.shortCuts.openByLinkText;
import static pageObject.Headers.Boilerplate.*;

@Listeners(TestListener.class)
public class BoilerplateTest extends Base implements Boilerplate {

    @BeforeMethod (alwaysRun = true)
    public void beforeMethod() throws Exception {
        logAndFindBoilerplateTab();
        findFisrtBoilerplate();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass(){
        close();
    }

    @Test (groups = {"positive", "sanity"}, enabled = false)
    public void addCustomABoilerplate() throws Exception{
        openByLinkText("Add custom Boilerplate");
        BaseObjects.setNameField(name);
        BaseObjects.setPackageName(packageName);
        BaseObjects.pressCreateButton();
        titleName.shouldHave(text(name));
    }

    @Test (groups = {"negative"})
    public void alreadyUsedBoilerpalte() throws Exception {
        getUsedItems();
        openByLinkText("Boilerplates");
        openByLinkText("Add custom Boilerplate");
        BaseObjects.setNameField(usedItems.get(0));
        BaseObjects.setPackageName(usedItems.get(1));
        BaseObjects.pressCreateButton();
        nameAlreadyUsed.shouldHave(text("Boilerplate with this Name already exists."));
    }

    @Test (groups = {"positive"})
    public void addId() throws Exception{
        setIdentifier(boilerplateID);
        setDescriptionField(description);
        BaseObjects.pressSaveButton();
        infoTooltip.shouldHave(text("Boilerplate successfully updated"));
    }

    @Test (groups = {"positive"})
    public void renameBoilerplate() throws Exception{
        BaseObjects.setNameField(name);
        BaseObjects.pressSaveButton();
        titleName.shouldHave(text(name));
    }

    @Test (groups = {"positive"})
    public void renamingPackage() throws Exception{
        BaseObjects.setPackageName(packageName);
        BaseObjects.pressSaveButton();
        packageNameField.shouldHave(value(packageName));
    }

    @Test (groups = {"positive"})
    public void renamingID() throws Exception {
        setDescriptionField(boilerplateID);
        BaseObjects.pressSaveButton();
        descriptionField.shouldHave(value(boilerplateID));
    }

    @Test (groups = {"positive"})
    public void changeLicense() throws Exception {
        BaseObjects.setLicenseDropdown("Apache License 2.0");
        BaseObjects.pressSaveButton();
        dropdownItem.shouldHave(attribute("selected"));
    }

    @Test (groups = {"positive"})
    public void versions() throws Exception{
        sleep(2000);
        openByLinkText("Versions");
        sleep(2000);
        tabTitle.shouldHave(text("Versions"));
    }


    //    TODO add collaborators
//    TODO transfer to Organisation
    @Test (groups = {"positive"})
    public void sharingSettings() throws Exception{
        sleep(2000);
        openByLinkText("Sharing Settings");
        sleep(2000);
        collaborabotsInput.click();
        collaborabotsInputMessage.shouldHave(text("No results found"));
    }
}