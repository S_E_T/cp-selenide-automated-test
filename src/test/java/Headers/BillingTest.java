package Headers;

import log.TestListener;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObject.Headers.Billing;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.switchTo;
import static log.shortCuts.*;

@Listeners(TestListener.class)
public class BillingTest implements Billing{

    @BeforeMethod(alwaysRun = true)
    public void beforeClass() {
        logging();
        openByLinkText("Billing");
    }

    @AfterMethod (alwaysRun = true)
    public void AfterMethod(){
        close();
    }

//    TODO enable
    @Test (enabled = false)
    public void addCard() throws Exception {
        Billing.pressAddcard();
        switchTo().frame("stripe_checkout_app");
        Billing.textInModal.shouldHave(text("Add Credit card"));
    }

    @Test
    public void checikngLeftBarBilling() throws Exception {
        openByLinkText("Billing Information");
        assertURL("https://control.divio.com/account/billing/");
    }

    @Test
    public void testName() throws Exception {
        openByLinkText("Invoices");
        assertURL("https://control.divio.com/account/invoices/");
    }

//    TODO add tests with invoces
}
