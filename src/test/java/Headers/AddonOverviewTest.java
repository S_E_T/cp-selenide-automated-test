package Headers;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObject.Headers.Addons;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.sleep;
import static log.Values.name;
import static log.Values.packageName;
import static log.shortCuts.*;
import static pageObject.BaseObjects.pressCreateButton;
import static pageObject.BaseObjects.pressSaveButton;
import static pageObject.BaseObjects.setLicenseDropdown;
import static pageObject.BaseObjects.setNameField;
import static pageObject.BaseObjects.setPackageName;
import static pageObject.Headers.Addons.*;

@Listeners(TestListener.class)
public class AddonOverviewTest extends Base implements Addons {

    @BeforeMethod (alwaysRun = true)
    public void BeforeMethod() {
        logging();
        getFirstAddon();
    }

    @Test(groups = {"positive", "sanity"})
    public void addCustomAddon() throws Exception {
        openByLinkText("Addons");
        openByLinkText("Add custom Addon");
        setNameField(name);
        setPackageName(packageName);
        setLicenseDropdown("The Unlicense");
        pressCreateButton();
        titleName.shouldHave(text(name));
    }

    @Test(groups = {"negative", "sanity"})
    public void alreadyUsedAddon() throws Exception {
        getUsedItems();
        openByLinkText("Addons");
        sleep(1000);
        openByLinkText("Add custom Addon");
        setNameField(usedItems.get(0));
        setPackageName(usedItems.get(1));
        pressCreateButton();
        nameAlreadyUsed.shouldHave(text("Addon with this Package name already exists."));
    }

//    TODO GO PUBLIC
//    @Test(groups = {"positive", "sanity", "go-live"})
//    public void publishingAddon() throws Exception {
//        makeAddonPublic();
//        pressSaveButton();
//        publishedText.shouldHave(titleName("Your addon is public."));
//    }

    @Test(groups = {"positive", "sanity"})
    public void renameAddon() throws Exception {
        setNameField(name);
        pressSaveButton();
        titleName.shouldHave(text(name));
    }

    @Test(groups = {"positive", "sanity"})
    public void overridingAuthor() throws Exception {
        setAuthorName("Overriding Author1");
        pressSaveButton();
        authorNameField.shouldHave(value("Overriding Author1"));
    }

    @Test(groups = {"positive"})
    public void changeLicense() throws Exception {
        setLicenseDropdown("Apache License 2.0");
        pressSaveButton();
        selectedDropdownItem.shouldHave(attribute("selected"));
    }

    @Test(groups = {"positive", "sanity"})
    public void renamingPackage() throws Exception {
//        sleep(1000);
        setPackageName(packageName);
        pressSaveButton();
        packageName.equalsIgnoreCase(packageName);
    }

////  TODO input keys to textarea
//    @Test (groups = {"positive", "sanity"})
//    public void addingDescription() throws Exception{
//        logAndFindAddon();
//    }

    @Test(groups = {"positive", "sanity"})
    public void changingLicense() throws Exception {
        setLicenseDropdown("ISC License");
        pressSaveButton();
        licenseDropdown.should(have(value("10")));
        sleep(600);
        setLicenseDropdown("Artistic License 2.0");
        pressSaveButton();
        licenseDropdown.should(have(value("3")));
    }

    //  TODO
    @Test(groups = {"positive"})
    public void cancelButton() throws Exception {
        String expected = nameField.getAttribute("value");
        setNameField(name);
        openByLinkText("Cancel");
        getFirstAddon();
        String current = nameField.getAttribute("value");
        current.equalsIgnoreCase(expected);
    }

    //TODO add pictures files to promotion
    @Test
    public void pictures() throws Exception {

    }

    @Test(groups = {"positive"})
    public void promotionInfo_AddingYouTubeLink() throws Exception {
        openByLinkText("Promotional Information");
        addonsTabName.shouldHave(text("Promotional Information"));
        setYouTubeField("https://www.youtube.com/watch?v=4hpEnLtqUDg");
        pressSaveButton();
        youTubeField.shouldHave(value("https://www.youtube.com/watch?v=4hpEnLtqUDg"));
//        successMessage.shouldHave(text("Addon successfully updated")); TODO this should be present

    }

    @Test(groups = {"positive"})
    public void changePackageInfo() throws Exception {
        goToPackageInformation();
        setPackageName(packageName);
        pressSaveButton();
//        successMessage.shouldHave(titleName("Addon successfully updated")); TODO this should be present
        packageNameField.should(value(packageName));
    }

    @Test(groups = {"positive"})
    public void changePackageVersion() throws Exception {
        goToPackageInformation();
        versionOfPackage.selectOption("stable");
        pressSaveButton();
        selectedVersionOfPackage.shouldHave(attribute("selected"));
    }

    @Test(groups = {"positive"})
    public void versions() throws Exception {
        openByLinkText("Versions");
        addonsTabName.shouldHave(text("Versions"));
    }

    @Test(groups = {"positive"}, priority = 1)
    public void addCompotibleTypes() throws Exception {
        goToPackageInformation();
        selectCompatableProject();
        inputForContatableProject.setValue("ra");
        sujectedCompatables.click();
        pressSaveButton();
        selectedCompatable.shouldHave(text("RapidPro"));
    }

    @Test(groups = {"positive"})
    public void removeAllCompotibleTypes() throws Exception {
        goToPackageInformation();
        deleteAllCompatibleProjectsTypes();
        pressSaveButton();
        inputForContatableProject.shouldBe(empty);
    }

    @Test(groups = {"positive", "sanity"})
    public void AddURLs() throws Exception {
        setRepositoryURL("http://google.com");
        setProjectURL("http://google.com");
        setPypiURL("http://google.com");
        setDocsURL("http://google.com");
        pressSaveButton();
        repositoryURL.shouldHave(value("http://google.com"));
        projectURL.shouldHave(value("http://google.com"));
        pypiURL.shouldHave(value("http://google.com"));
        docsURL.shouldHave(value("http://google.com"));
    }

    @Test(groups = {"positive"}, priority = 1)
    public void EditURL() throws Exception {
        setRepositoryURL("http://google.com.ua");
        setProjectURL("http://google.com.ua");
        setPypiURL("http://google.com.ua");
        setDocsURL("http://google.com.ua");
        pressSaveButton();
        repositoryURL.shouldHave(value("http://google.com.ua"));
        projectURL.shouldHave(value("http://google.com.ua"));
        pypiURL.shouldHave(value("http://google.com.ua"));
        docsURL.shouldHave(value("http://google.com.ua"));
    }

    @Test(groups = {"positive"}, priority = 2)
    public void DeleteURLS() throws Exception {
        setRepositoryURL("http://google.com");
        setProjectURL("http://google.com");
        setPypiURL("http://google.com");
        setDocsURL("http://google.com");
        pressSaveButton();
        repositoryURL.shouldHave(value(""));
        projectURL.shouldHave(value(""));
        pypiURL.shouldHave(value(""));
        docsURL.shouldHave(value(""));
    }

    //    TODO add collaborators
//    TODO transfer to Organisation
    @Test(groups = {"positive"})
    public void sharingSettings() throws Exception {
        openByLinkText("Sharing Settings");
        openCollaboratorsDropdown();
        collaboratorsResult.shouldHave(text("No results found"));
    }

    @Test(groups = {"positive"})
    public void visitAddonsMarketPlace() throws Exception {
        openByLinkText("Addons");
        openByLinkText("Visit Addons Marketplace");
        assertTitle("AddonsTest - django CMS Marketplace");
    }

    @Test(groups = {"positive"})
    public void visitControlPanel() throws Exception {
        openByLinkText("Divio Control Panel");
        assertURL("https://control.divio.com/control/");
    }

    @Test(groups = {"positive"})
    public void visitMarketplace() throws Exception {
        openByLinkText("django CMS marketplace");
        assertURL("https://marketplace.django-cms.org/en/");
    }
}