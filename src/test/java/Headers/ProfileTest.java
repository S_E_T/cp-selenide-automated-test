package Headers;

import baseclass.Base;
import com.codeborne.selenide.ElementsCollection;
import log.TestListener;
import org.openqa.selenium.Keys;
import org.testng.annotations.*;
import pageObject.Headers.Profile;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static log.Values.*;
import static log.Values.name;
import static log.shortCuts.assertURL;
import static log.shortCuts.openByLinkText;
import static org.testng.Assert.*;
import static pageObject.BaseObjects.pressSaveButton;
import static pageObject.Headers.Profile.*;

@Listeners(TestListener.class)
public class ProfileTest extends Base implements Profile {

//    TODO devide into separate classes

    @BeforeMethod(alwaysRun = true)
    public void beforeClass() throws Exception {
        logAndFindProfileTab();
    }

    //    TODO add the check of rightsidebar

    @Test (groups = {"positive", "sanity"})
    public void inputWebsiteValid() throws Exception{
        setUrlField("http://google.com");
        pressSaveButton();
        urlField.shouldHave(value("http://google.com"));
    }

    @Test (groups = {"negative"})
    public void inputWebsiteInvalid() throws Exception {
        String current = urlField.getValue();
        setUrlField(name);
        String actual = urlField.getValue();
        pressSaveButton();
        current.equalsIgnoreCase(actual);
    }

    @Test (groups = {"positive"})
    public void inputContactEmailValid() throws Exception {
        setEmailField(validLogin);
        pressSaveButton();
        emailField.shouldHave(value(validLogin));
    }

    //    TODO add validation assertion
    @Test (groups = {"negative"})
    public void inputContactEmailInvalid() throws Exception {
        setEmailField(name);
        pressSaveButton();
    }

    //TODO
    @Test (groups = {"positive"}, enabled = false)
    public void inputDescriptionValid()  throws Exception{
        switchTo().innerFrame("cke_wysiwyg_frame.cke_reset");
//        switchTo().frame("cke_wysiwyg_frame.cke_reset");
        setDescriptionField(description);
        pressSaveButton();
        descriptionField.shouldHave(attribute(description));
    }

    //    TODO assert
    @Test (groups = {"positive"}, enabled = false)
    public void addProfilePicture()  throws Exception{
        deleteAvatarPicture();
        uploadAvatarPiture();
        pressSaveButton();
    }

    //    TODO assert
    @Test (groups = {"positive"}, enabled = false)
    public void addCoverPicture()  throws Exception{
        deleteCoverPicture();
        uploadCoverPicture();
        pressSaveButton();
    }

//    TODO Assert is not working
    @Test (groups = {"positive"}, enabled = false)
    public void addLocations()  {
        GoToLocations();
        openByLinkText("Add Personal Address");
        setNewLocation("Moscow");
        selectSuggestion();
        pressAddButton();
        assertTrue(locationsArray.toString().contains("Moscow"));
    }

    @Test (groups = {"positive"})
    public void cancelAddingLocation() throws Exception {
        GoToLocations();
        openByLinkText("Add Personal Address");
        setNewLocation("Moscow");
        selectSuggestion();
        pressCancelInModal();
        locationField.shouldNotBe(visible);
    }

    //TODO switch to modal
    @Test (groups = {"positive"})
    public void changingLocation() throws Exception {
        GoToLocations();
        pressChange();
        pressEdit();
        setNewLocation("New York");
        pressAddButton();
        locationsArray.findBy(text("New York"));
    }

    //    TODO Assert is not working
    @Test (groups = {"positive"}, enabled = false)
    public void removingLocationButton() throws Exception {
        GoToLocations();
        ElementsCollection should = $$(".location-address");
        DeleteLocation();
        ElementsCollection now = $$(".location-address");
        assertNotEquals(should.size(), now.size());
    }


    @Test (groups = {"positive"})
    public void expertiseSelectingOneChecked() throws Exception {
        goToAddInfo();
        makeSureThatNotSelected();
        selectExpertise();
        pressSaveButton();
        expertise.shouldHave(cssClass("checked")) ;
    }

    @Test (groups = {"positive"}, dependsOnMethods = "expertiseSelectingOneChecked")
    public void expertiseSelectingUncheck() throws Exception {
        goToAddInfo();
        selectExpertise();
        pressSaveButton();
        expertise.shouldHave(cssClass(""));
    }

    //    TODO
    @Test (groups = {"positive"})
    public void addingTags() throws Exception {
        goToAddInfo();
        tagsField.setValue("Tag");
        tagsField.pressEnter();
        pressSaveButton();
        $(".tag.label.label-info").shouldHave(text("Tag"));
    }

    //    TODO
    @Test (groups = {"positive"}, dependsOnMethods = {"addingTags"})
    public void removingTags() throws Exception{
        goToAddInfo();
        tagsField.click();
        tagsField.sendKeys(Keys.BACK_SPACE);
        pressSaveButton();
        tagsField.shouldBe(empty);
    }

    @Test (groups = {"positive"})
    public void activeYear() throws Exception{
        goToAddInfo();
        setDropdown(activeSinse, "2004");
        pressSaveButton();
        sixteenthOption.shouldHave(attribute("selected"));
    }

    @Test (groups = {"positive"})
    public void activeYearChange() throws Exception{
        goToAddInfo();
        setDropdown(activeSinse, "2000");
        pressSaveButton();
        setDropdown(activeSinse, "1990");
        pressSaveButton();
        secondOption.shouldHave(attribute("selected"));
    }

    @Test (groups = {"positive"})
    public void removingYearChange() throws Exception{
        goToAddInfo();
        setDropdown(activeSinse, "2000");
        pressSaveButton();
        setDropdown(activeSinse, "Choose year");
        pressSaveButton();
        firstOption.shouldHave(attribute("selected"));
    }

    @Test (groups = {"positive"})
    public void twitterAdding() throws Exception{
        goToAddInfo();
        setTwitterField("twit");
        pressSaveButton();
        twitterField.shouldHave(value("twit"));
    }

    @Test (groups = {"positive"}, dependsOnMethods = {"twitterAdding"})
    public void twitterDeleting() throws Exception{
        goToAddInfo();
        setTwitterField("");
        pressSaveButton();
        twitterField.shouldHave(value(""));
    }

    //    TODO add org
    @Test (enabled = false)
    public void networkOrg() throws Exception{

    }

    @Test (groups = {"positive"})
    public void cancelButton()throws Exception {
        goToAddInfo();
        setTwitterField("Twitter");
        pressSaveButton();
        setTwitterField("Twitter2");
        pressing(canceling);
        twitterField.shouldHave(value("Twitter"));
    }

    //    TODO Assert is not working
    @Test (groups = {"positive"}, enabled = false)
    public void AddingPortfolioItem() throws Exception {
        goToPorfolio();
        openByLinkText("Add Portfolio Item");
        setTitleField(portfolioName);
        urlField.setValue("http://example.com").pressEnter();
        assertTrue(portfolioItems.toString().contains(portfolioName));
    }

    //TODO assert
    @Test (groups = {"positive"})
    public void DoneWithCMS() throws Exception {
        goToPorfolio();
        openByLinkText("Add Portfolio Item");
        setTitleField(portfolioName + "DoneWithCMS");
        setUrlField("http://example.com");
        pressing(builtWithDjango);
        urlField.sendKeys(Keys.ENTER);
        System.out.println(portfolioItems.listIterator());
    }

//TODO assert
    @Test (groups = {"positive"}, enabled = false)
    public void addingScreen() throws Exception{
        goToPorfolio();
        openByLinkText("Add Portfolio Item");
        setTitleField(portfolioName);
        setUrlField("http://example.com");
        uploadScreenshot(file);
        pressSaveButton();
        assertTrue(portfolioItems.toString().contains(portfolioName));
    }

    //TODO assert
    @Test (groups = {"positive"})
    public void linkInPortfolio() throws Exception{
        goToPorfolio();
        openByLinkText("Add Portfolio Item");
        openByLinkText("django-cms.org");
        sleep(5000);
        assertURL("https://www.django-cms.org/en/");
    }

    //    TODO Assert is not working
    @Test (groups = {"positive"}, enabled = false)
    public void EditingPorfolioItem() throws Exception {
        goToPorfolio();
        pressEditIcon();
        refreshScreenshot();
        setTitleField(portfolioName + "-edited");
        urlField.sendKeys(Keys.ENTER);
        assertTrue(portfolioItems.toString().contains(portfolioName + "-edited"));
    }

    //TODO assert
    @Test (groups = {"positive"})
    public void CancelDeletingPorfolioItem() throws Exception {
        goToPorfolio();
        String actual = itemsName.getText();
        pressTrashIcon();
        pressCancelInModal();
        String expected = itemsName.getText();
        assertEquals(actual, expected);
    }

    //TODO assert
    @Test (groups = {"positive"})
    public void DeletingPorfolioItem()  throws Exception {
        goToPorfolio();
        String actual = itemsName.getText();
        pressTrashIcon();
        pressDeleteButton();
        String expected = itemsName.getText();
        assertNotEquals(actual, expected);
    }
}