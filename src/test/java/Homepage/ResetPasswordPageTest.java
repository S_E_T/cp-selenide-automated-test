package Homepage;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObject.homePage.resetPage;

import static com.codeborne.selenide.Selenide.open;
import static log.Values.validLogin;
import static log.shortCuts.assertURL;
import static log.shortCuts.openByLinkText;
import static pageObject.homePage.registerPageObject.setEmailField;
import static pageObject.homePage.resetPage.messageContains;
import static pageObject.homePage.resetPage.pressReset;

@Listeners(TestListener.class)
public class ResetPasswordPageTest extends Base implements resetPage {

    @BeforeMethod
    public void before() {
        open(ResetPasswordPage);
    }

    @Test(groups = {"positive", "sanity"})
    public void testResetPassword() {
        setEmailField(validLogin);
        pressReset();
        messageContains("Don’t forget to check your SPAM-folder.");
    }

    @Test(groups = {"negative"})
    public void testBlankInput() {
        setEmailField("");
        pressReset();
        assertURL("https://control.divio.com/auth/account/reset-password/");
    }

    @Test(groups = {"negative"})
    public void testInvalidFormatEmail() {
        setEmailField("asd");
        pressReset();
        assertURL("https://control.divio.com/auth/account/reset-password/");
    }

    @Test(groups = {"positive"})
    public void testLogInLink() throws Exception {
        openByLinkText("Log in");
        assertURL("https://control.divio.com/auth/login/");
    }

    @Test(groups = {"positive"})
    public void testSignInLink() throws Exception {
        openByLinkText("Sign up for free");
        assertURL("https://control.divio.com/auth/login/");
    }
}
