package Homepage;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObject.homePage.registerPageObject;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static log.Values.newUserLogin;
import static log.Values.validLogin;
import static log.shortCuts.assertURL;
import static log.shortCuts.openByLinkText;
import static pageObject.homePage.loginPageObject.clickOnLogo;
import static pageObject.homePage.loginPageObject.googleLogo;
import static pageObject.homePage.loginPageObject.signInWithGoogle;
import static pageObject.homePage.registerPageObject.*;

@Listeners(TestListener.class)
public class RegisterPageTest extends Base implements registerPageObject {

//    TODO move "NEW USER LOGIN"

    @BeforeMethod
    public void before() {
        open(RegisterPage);
    }

    @AfterClass (alwaysRun = true)
    public void AfterClass() {
        close();
    }

    @Test(groups = {"positive", "sanity"})
    public void testRegistrationEmail() throws Exception {
        setEmailField(newUserLogin);
        registerPageObject.pressCreateButton();
        sleep(3000);
        messageIsPreset("An e-mail with further instructions has been sent to:");
    }

    @Test(groups = {"negative", "sanity"})
    public void UsedEmail() throws Exception {
        setEmailField(validLogin);
        registerPageObject.pressCreateButton();
        popUpIsPreset("This email address has already been used.");
    }

    //    TODO
    @Test(groups = {"negative", "sanity"})
    public void testBlankInput() throws Exception {
        setEmailField("");
        registerPageObject.pressCreateButton();
        assertURL("https://control.divio.com/auth/register/");
    }

    @Test(groups = {"negative", "sanity"})
    public void testInvalidEmail() throws Exception {
        setEmailField("sergiy.sydorenko-divio.com");
        registerPageObject.pressCreateButton();
        assertURL("https://control.divio.com/auth/register/");
    }

    @Test(groups = {"positive"})
    public void testGoogleLink() throws Exception {
        signInWithGoogle();
        googleLogo.isDisplayed();
    }

    @Test(groups = {"positive"})
    public void testCheckLogo() throws Exception {
        clickOnLogo();
        assertURL("https://www.divio.com/en/");
    }

    @Test(groups = {"positive"})
    public void testLoginLink() throws Exception {
        openByLinkText("Login");
        assertURL("https://control.divio.com/auth/login/");
    }

}
