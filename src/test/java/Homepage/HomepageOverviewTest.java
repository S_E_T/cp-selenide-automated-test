package Homepage;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.*;
import pageObject.homePage.homepageObject;
import pageObject.homePage.loginPageObject;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static log.shortCuts.assertURL;
import static log.shortCuts.logging;
import static log.shortCuts.openByLinkText;
import static org.openqa.selenium.By.partialLinkText;
import static org.openqa.selenium.By.xpath;
import static pageObject.homePage.homepageObject.*;

@Listeners(TestListener.class)
public class HomepageOverviewTest extends Base implements homepageObject {

    @BeforeMethod(alwaysRun = true)
    public void BeforeMethod() {
        logging();
        open(homePageDev);
    }

    //    TODO
    @Test
    public void dropdownOverview() {
        openContextDropdown();
        $(partialLinkText("Add New Organisation")).shouldHave(text("Add New Organisation"));
        $(partialLinkText("Manage Organisations")).shouldHave(text("Manage Organisations"));
    }

    //    TODO
    @Test
    public void dropdownOverviewManageOrg() {
        openContextDropdown();
        openByLinkText("Manage Organisations");
        assertURL("https://control.divio.com/account/organisations/");
    }

    //TODO
    @Test (enabled = false)
    public void dropdownOverviewAddOrg() {
        openContextDropdown();
        openByLinkText("Add New Organisation");
        switchTo().innerFrame(".modal-dialog.modal-dialog-form");
        $(".h4.titleName-primary.js-add-collaborator-row").shouldBe(visible);
    }

    @Test(dataProvider = "tabs", dataProviderClass = homepageObject.class)
    public void tabChecking(String TabName, String url) {
        openByLinkText(TabName);
        assertURL(url);
    }

    @Test
    public void search() {
        openSearchDropdown();
        $("#search").shouldBe(visible);
    }

//    TODO searching

    //TODO
    @Test(dataProvider = "helpDropdown", dataProviderClass = homepageObject.class, enabled = false)
    public void status(String itemName, String url) {
        openHelpDropdown();
        $(xpath(itemName)).click();
        switchTo().window(1);
        assertURL(url);
    }

    //    TODO
    @Test (enabled = false)
    public void tooltips() {
        openTooltip();
        tooltipText.shouldHave(text("The Divio Control Panel"));
    }

    @Test
    public void accountDropdownIsOpened() {
        openAccountDropdown();
        dropdownSign.shouldHave(text("Signed in as"));
    }

    @Test(dataProvider = "accountDropdown", dataProviderClass = homepageObject.class)
    public void productChanges(String link, String url) {
        openAccountDropdown();
        openByLinkText(link);
        assertURL(url);
    }

    @Test
    public void loggingOut() {
        openAccountDropdown();
        openByLinkText("Logout");
        assertURL("http://control.divio.com/auth/login");
    }

    @Test(dataProvider = "links", dataProviderClass = loginPageObject.class)
    public void testLinks(String LinkText, String Naming) throws Exception {
        openByLinkText(LinkText);
        switchTo().window(1);
        assertName(Naming);
        close();
        logging();
    }

    @Test
    public void addProjectLink() {
        openByLinkText("Add new Project");
        assertURL("https://control.divio.com/control/project/create/");
    }

}