package Homepage;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObject.homePage.loginPageObject;

import static com.codeborne.selenide.Selenide.open;
import static log.Values.validLogin;
import static log.Values.validPassword;
import static log.shortCuts.*;
import static pageObject.homePage.loginPageObject.*;

@Listeners(TestListener.class)
public class LoginPageTest extends Base implements loginPageObject {

    @BeforeMethod(alwaysRun = true)
    public void BeforeMethod() {
        open(homePageDev);
    }

    @Test(groups = {"positive", "sanity"})
    public void basicLogin() {
        setUserName(validLogin);
        setPassword(validPassword);
        pressSubmitButton();
        assertURL("https://control.divio.com/control/");
        logout();
        CenterTextIsValid();
    }

    @Test
    public void LoginWithBlankFields() {
        usernameField.clear();
        passwordField.clear();
        pressSubmitButton();
        CenterTextIsValid();
    }

    @Test
    public void LoginWithEmailOnly() throws Exception {
        setUserName(validLogin);
        pressSubmitButton();
        CenterTextIsValid();
    }

    @Test
    public void LoginWithPasswordOnly() throws Exception {
        setPassword(validPassword);
        pressSubmitButton();
        CenterTextIsValid();
    }

    @Test
    public void LoginReverse() throws Exception {
        setUserName(validPassword);
        setPassword(validLogin);
        pressSubmitButton();
        CenterTextIsValid();
    }

    @Test (enabled = false)
    public void UnregisteredUser() throws Exception {
        setUserName("123@asd.asd");
        setPassword("123123123");
        pressSubmitButton();
        validationErrorMessageIsPresent();
    }

    @Test
    public void GoogleLink() throws Exception {
        signInWithGoogle();
        googleLogo.isDisplayed();
    }

    @Test
    public void GitHubLink() throws Exception {
        signInWithGitHib();
        assertTitle("Sign in to GitHub · GitHub");
    }

    @Test
    public void SingInLink() throws Exception {
        openByLinkText("Sign up for free");
        assertURL("https://control.divio.com/auth/register/");
    }

    @Test
    public void ForgotPasswordLink() throws Exception {
        openByLinkText("Forgot password?");
        assertURL("https://control.divio.com/auth/account/reset-password/");
    }

    @Test
    public void CheckLogo() throws Exception {
        clickOnLogo();
        assertURL("https://www.divio.com/en/");
    }
}