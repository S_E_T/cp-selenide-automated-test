//package ProjectCreation;
//
//import baseclass.Base;
//import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.Test;
//
//import static com.codeborne.selenide.Condition.titleName;
//import static com.codeborne.selenide.Selenide.$;
//import static com.codeborne.selenide.Selenide.title;
//import static log.Elements.projectNameField;
//import static log.Values.projectName;
//import static log.shortCuts.logging;
//import static org.openqa.selenium.By.*;
//

//@Listeners(TestListener.class)
//public class AddingNewProject extends Base {
//
//    private void pressAddButton(){
//        logging();
//        $(partialLinkText("Add new Project")).click();
//    }
//
//    //  TODO  Think how to find the project
//    @Test
//    public void testAddNewProject() throws Exception {
//        pressAddButton();
//        projectNameField.setValue(projectName);
//        projectNameField.submit();
//        new Select($(id("project-context"))).selectByVisibleText("compatableProjectsItems me2");
//        $(xpath("(//button[@type='submit'])[5]")).click();
//        $(".btn.btn-default.btn-context-toggle.dropdown-toggle").click();
//        $(partialLinkText("compatableProjectsItems me2")).click();
//        $(tagName("title")).shouldHave(titleName(projectName));
//    }
//
//    //    TODO review xpath
//    @Test
//    public void testContextTooltip() throws Exception {
//        pressAddButton();
//        $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/form/div[1]/div[2]/div/a/i")).click();
////        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
//        $("div.popover-content > p").shouldHave(titleName("A project can belongs to an individual or an organisation. Here you can attach your project directly to your personal account or to an organisation you’re part of. Set up a new organisation."));
////        $(By.xpath("html/body/div[2]/div[2]/div/div[2]/div/div[1]/form/div[1]/div[2]/div/a/i")).click();
//        $(linkText("Set up a new organisation")).click();
//        title().equalsIgnoreCase("https://control.divio.com/account/organisations/add-new/");
//    }
//
//}
