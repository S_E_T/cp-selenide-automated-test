package ProjectLeftBar;

import baseclass.Base;
import com.codeborne.selenide.SelenideElement;
import log.TestListener;
import org.openqa.selenium.By;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.matchText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static java.lang.String.valueOf;
import static log.shortCuts.logging;

@Listeners(TestListener.class)
public class DashboardTest extends Base {

//    TODO


    @Test (enabled = false)
    public void nameMatching() {
        logging();
        open("https://control.divio.com/control/1073/");

        SelenideElement nameOnOrgScreen = $(By.tagName("title"));
        nameOnOrgScreen.getText();
        $(".img-responsive").click();

        SelenideElement nameOnDetailedPage = $(By.tagName(".control-title"));
        nameOnOrgScreen.should(matchText(valueOf(nameOnDetailedPage)));
    }
}
