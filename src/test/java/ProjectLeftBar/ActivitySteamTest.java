package ProjectLeftBar;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.*;
import pageObject.ProjectLeftSideBar.ActivityStream;

import static com.codeborne.selenide.Condition.text;
import static log.shortCuts.logging;
import static log.shortCuts.openByLinkText;
import static pageObject.ProjectLeftSideBar.ActivityStream.*;
import static pageObject.ProjectLeftSideBar.Addons.DeployTestServer;

@Listeners(TestListener.class)
public class ActivitySteamTest extends Base implements ActivityStream {

    @BeforeMethod
    public void beforeClass() {
        logging();
        getProject();
    }

    @Test
    public void nameMatching() {
        openByLinkText("Activity Stream");
        header.shouldHave(text("Activity Stream"));
    }

    @Test
    public void checkLog() {
        goToAddons();
        configureAnAddon();
        openByLinkText("Dashboard");
        DeployTestServer();
        openByLinkText("Activity Stream");
        openCheckLog();
        windowIsOpenedAndHasAText();
    }

    @Test (enabled = false)
    public void showGitDiff() {
        goToAddons();
        configureAnAddon();
        openByLinkText("Dashboard");
        DeployTestServer();
        openByLinkText("Activity Stream");
        openShowDiff();
        pressDiffButton();
        changeArePresent();
    }
}
