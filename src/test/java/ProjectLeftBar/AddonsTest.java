package ProjectLeftBar;

import baseclass.Base;
import log.TestListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static log.shortCuts.logging;
import static org.openqa.selenium.By.partialLinkText;
import static org.openqa.selenium.By.xpath;

@Listeners(TestListener.class)
public class AddonsTest extends Base {

//    TODO




    @Test (enabled = false)
    public void basic() {
        logging();
        open("https://control.divio.com/control/1073/");
        $(".img-responsive").click();
        $(partialLinkText(" AddonsTest")).click();
        $(".panel-title.panel-title-offset.h2.pull-left").shouldHave(text("django CMS and Django AddonsTest"));
    }

    @Test (enabled = false)
    public void searching() {
        logging();
        open("https://control.divio.com/control/1073/");
        $(".img-responsive").click();

        $("#search").setValue("faq");
        $(".spacer-zero").shouldHave(text("No entries matched the filter. Switch back to view all addons."));
    }

    @Test (enabled = false)
    public void invalidSearch() {
        logging();
        open("https://control.divio.com/control/1073/");
        $(".img-responsive").click();

        $("#search").setValue("qweqweqweeqw");
        $(xpath("html/body/div[2]/div[2]/div/div[2]/div/div/ul/form/li[24]/div/div[1]/h3")).shouldHave(text("Aldryn FAQ"));
    }

    @Test (enabled = false)
    public void linkInsidInvalidSearch() {
        logging();
        open("https://control.divio.com/control/1073/");
        $(".img-responsive").click();

        $("#search").setValue("qweqweqweeqw");
        $(".js-trigger-filter").exists();
        $(".js-trigger-filter").click();
//        titleName shpuld be present on page  "django CMS" in some plugin
    }


    @Test
    public void installingAddon() {
    }

    @Test
    public void configureAddon() {
    }

    @Test
    public void updateAddonToLatest() {
    }

    @Test
    public void deletingAddon() {
    }

    @Test
    public void addCustonAddon() {
    }

    @Test
    public void tabsType() {
    }

    @Test
    public void tabsStatus() {
    }
}
