package ProjectLeftBar;

import baseclass.Base;
import log.TestListener;
import org.junit.Test;
import org.testng.annotations.Listeners;
import pageObject.BaseObjects;
import pageObject.ProjectLeftSideBar.ActivityStream;
import pageObject.ProjectLeftSideBar.GeneralSettings;

import static com.codeborne.selenide.Condition.present;

@Listeners(TestListener.class)
public class GeneralSettingsTest extends Base implements GeneralSettings {

    @Test
    public void realeseChannel() {
            setDropdown(releaseChannen, "Python 3");
            ActivityStream.cautionText.shouldBe(present);
            BaseObjects.pressSaveButton();
            ActivityStream.succesfulMessage.shouldBe(present);
    }
}
